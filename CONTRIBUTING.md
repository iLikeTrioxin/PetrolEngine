####This file describes how you can contribute to this repository.

#I describe here those topics:
- ####General contribution info
- ####Preferred code style      



#General info

So to begin with, I'm open to any suggestions
that can help improve my code or knowledge.

If you see any bugs or things I could do better
please commit your change or tell me about it
on discord or any other media (can be found on https://www.yukiteru.xyz)


# Preferred code style


##before:
- lower camel case (ex. lowerCamelCase)
- upper camel case (ex. UpperCamelCase) so called pascal case
- all caps    case (ex. ALLCAPSCASE)


###Variable names are written with lower camel case.
```c++
int someVariable;
```

###Class names are written using upper camel case.
```c++
class SomeClass
```

###Files and directories are also named upper camel case.
```
PetrolEngine/Deps/Xyz.cpp
```

###Macros are all caps with words seperated with '_'
```c++
LOG("some info message", 1);
```

###Internal includes like Renderer/Renderer.h are in ""
```c++
#include "Renderer/Renderer.h"
```

###External includes like GLFW/glfw3.h are in <>
```c++
#include <GLFW/glfw3.h>
```

#Future

## Code
### My goto code style is something like:
###
```c++
// Renderer and Its resources are build in such a way that
// you can create your own instance of resource manager
// and override Its functions to simply implement your own API (Vulkan and OpenGL are builtin)
class DirectX: public RendererResourceManager{
public:
    virtual RendererAPI createRenderer() override { return * (RendererAPI*) &DirectXRenderer(); };
    virtual Shader      createShader  () override { return * (Shader     *) &DirectXShader  (); };
    ...
};

Renderer *renderer = new Renderer<DirectX>(...);
Shader   *shader   = new Shader<DirectX>(...);
Window   *window   = new Window(GLFW, ...);
Model    *model    = new Model(...);

while( !window->shouldClose() ) {
    // there's no need to complicate function calls, so
    // function should have 'low level' and 'high level' overloads
    renderer->draw(model, shader);
    renderer->draw(model.vertexArray, shader);
    
    window->swapBuffers();
}

// delete can also be overloaded if needed, for example
// you need to delete vertex buffer before deleting model
delete renderer;
delete window;
delete shader;
delete model;
```