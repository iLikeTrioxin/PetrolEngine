#pragma once

#include "../VertexBuffer.h"

namespace PetrolEngine {
	class OpenGLVertexBuffer : public VertexBuffer {
	public:
		explicit OpenGLVertexBuffer(VertexLayout layout, const void *data = nullptr, int64 size = 0);

        void setData(const void* data, int64 size) override;

		~OpenGLVertexBuffer() override;
	};
}