#include <PCH.h>

#include <utility>

#include "OpenGLVertexBuffer.h"

namespace PetrolEngine {
	OpenGLVertexBuffer::OpenGLVertexBuffer(VertexLayout layout, const void *data, int64 size) { LOG_FUNCTION();
		this->layout = std::move(layout);

        glGenBuffers(1, &ID);

        if (data == nullptr) return;

        glBindBuffer(GL_ARRAY_BUFFER, ID);
        glBufferData(GL_ARRAY_BUFFER, size, data, GL_STATIC_DRAW);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
    }

	void OpenGLVertexBuffer::setData(const void* data, int64 size) { LOG_FUNCTION();
		glBindBuffer(GL_ARRAY_BUFFER, ID);
		glBufferData(GL_ARRAY_BUFFER, size, data, GL_STATIC_DRAW);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
	}

	OpenGLVertexBuffer::~OpenGLVertexBuffer() { LOG_FUNCTION();
		glDeleteBuffers(1, &ID);
	}
}