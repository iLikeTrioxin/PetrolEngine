#include <PCH.h>

#include "OpenGLIndexBuffer.h"

namespace PetrolEngine {
	OpenGLIndexBuffer::OpenGLIndexBuffer(const void* data, int64 size) { LOG_FUNCTION();
        this->size = size / (int64) sizeof(int);

		glGenBuffers(1, &ID);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ID);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, size, data, GL_STATIC_DRAW);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	}

    // TODO: reconsider assuming that size is always type of int.
	void OpenGLIndexBuffer::setData(const void* data, int64 size) { LOG_FUNCTION();
		this->size = size / (int64) sizeof(int);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ID);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, size, data, GL_STATIC_DRAW);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    }

	OpenGLIndexBuffer::~OpenGLIndexBuffer() { LOG_FUNCTION();
		glDeleteBuffers(1, &ID);
	}
}