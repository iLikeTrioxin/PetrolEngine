#include <PCH.h>

#include "OpenGLRenderer.h"
#include "../Texture.h"
#include "../../Core/Files.h"
#include "../Text.h"

namespace PetrolEngine {

	void OpenGLRenderer::getDeviceConstantValue(DeviceConstant deviceConstant, void* outputBuffer) {
		auto openGLDeviceConstant = openGLDeviceConstants.find(deviceConstant);

		glGetIntegerv(openGLDeviceConstant->second, (GLint*) outputBuffer);
	}

	void OpenGLRenderer::setViewport(int x, int y, int width, int height) {
		glViewport(x, y, width, height);
	}

	int OpenGLRenderer::init(bool debug) {
        LOG("Initializing OpenGL.", 1);

		if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress)) {
            LOG("Initializing OpenGL failed.", 3);
			return 1;
		}

		glEnable(GL_DEPTH_TEST); //-V525
		glEnable(GL_CULL_FACE);
		glCullFace(GL_BACK);

		return 0;
	}
    void OpenGLRenderer::renderText(const String& text, Transform &transform) {
        float x = transform.position.x;
        float y = transform.position.y;

        // iterate through all characters
        String::const_iterator c;
        for (c = text.begin(); c != text.end(); ++c) {
            Text::Character ch = Text::get(*c);

            float xPos = x + (float) ch.Bearing.x * transform.scale.x;
            float yPos = y - (float) (ch.Size.y - ch.Bearing.y) * transform.scale.y;

            float w = (float) ch.Size.x * transform.scale.x;
            float h = (float) ch.Size.y * transform.scale.y;

            drawQuad2D(ch.texture, Transform({xPos, yPos, 0.f}, {w, h, 1.f}));

            x += (float) (ch.Advance >> 6) * transform.scale.x; // bitshift by 6 to get value in pixels (2^6 = 64)
        }

        flush();
    }
	/*
	void OpenGLRenderer::renderText(const std::string& text, Transform& transform) {
		LOG_FUNCTION();

		auto shader = Shader::load(
			"textShader",
			ReadFile("../Hei/Resources/Shaders/textShader.vert"),
			ReadFile("../Hei/Resources/Shaders/textShader.frag")
		);

		glUseProgram(shader->ID);

		static auto perv = glm::ortho(0.0f, 800.f, 0.0f, 500.f);
		
		{
			LOG_SCOPE("Setting shader values.");

			shader->setMat4("projection", perv);
			shader->setInt("text", 0);
		}

		glActiveTexture(GL_TEXTURE0);

		static Vector<Vertex> squareVertices({{}, {}, {}, {}, {}, {} });
		static Vector< uint > squareIndices ({ 0,  1,  2,  3,  4,  5  });
		static Mesh characterMesh = Mesh(squareVertices, squareIndices, Material());

		float x = transform.position.x;
		float y = transform.position.y;

		// iterate through all characters
		std::string::const_iterator c;
		for (c = text.begin(); c != text.end(); ++c) {
			LOG_SCOPE("Rendering character");

			Text::Character ch = Text::get(*c);
			
			glBindTexture(GL_TEXTURE_2D, ch.texture->getID());
			
			float xPos = x + (float) ch.Bearing.x * transform.scale.x;
			float yPos = y - (float) (ch.Size.y - ch.Bearing.y) * transform.scale.y;

			float w = (float) ch.Size.x * transform.scale.x;
			float h = (float) ch.Size.y * transform.scale.y;

			// update VBO for each character

            // + 0 are used here to align arguments in clion editor
            // it will be stripped by compiler anyway
            squareVertices = {
				Vertex( {xPos + 0, yPos + h, 0.f}, {0.f, 0.f} ),
				Vertex( {xPos + 0, yPos + 0, 0.f}, {0.f, 1.f} ),
				Vertex( {xPos + w, yPos + 0, 0.f}, {1.f, 1.f} ),
				Vertex( {xPos + 0, yPos + h, 0.f}, {0.f, 0.f} ),
				Vertex( {xPos + w, yPos + 0, 0.f}, {1.f, 1.f} ),
				Vertex( {xPos + w, yPos + h, 0.f}, {1.f, 0.f} )
			};

			characterMesh.vertexBuffer->setData(squareVertices.data(), squareVertices.size() * sizeof(Vertex));
			
			glBindVertexArray(characterMesh.vertexArray->getID());

			//glBindBuffer(GL_ARRAY_BUFFER, a.vertexBuffer->getID());
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, characterMesh.indexBuffer->getID());
			
			glDrawElements(GL_TRIANGLES, (int) characterMesh.indexBuffer->getSize(), GL_UNSIGNED_INT, nullptr);

			// now advance cursors for next glyph (note that advance is number of 1/64 pixels)
			x += (float) (ch.Advance >> 6) * transform.scale.x; // bitshift by 6 to get value in pixels (2^6 = 64)
		}
	}*/

	void OpenGLRenderer::renderMesh(Mesh& mesh, Transform& transform, Camera& camera) { LOG_FUNCTION();
		auto shader = mesh.material.shader;
        Renderer::getErrors();

		glUseProgram(shader->ID);
		
        // Applying them to shader used by mesh
		shader->setMat4("model", transform.transformation);
		shader->setMat4("pav"  , camera.perspective * camera.view);
		
		shader->setInt  ( "material.diffuse"  , 0   );
		shader->setInt  ( "material.specular" , 0   );
		shader->setFloat( "material.shininess", 1.f );

		shader->setInt  ( "light[0].lightType",  1                 );
		shader->setVec3 ( "light[0].direction", -1.0f,  0.0f, 1.0f );
		shader->setVec3 ( "light[0].ambient"  ,  0.2f,  0.2f, 0.2f );
		shader->setVec3 ( "light[0].diffuse"  ,  1.0f,  1.0f, 1.0f );
		shader->setVec3 ( "light[0].specular" ,  0.0f,  0.0f, 0.0f );

		// if (currentShader != mesh.material.shader->ID) {
		// 	mesh.material.shader->use();
		// 	currentShader = mesh.material.shader->ID;
		// }

		// loading textures into the buffers
		uint32 diffuseNumber  = 1;
		uint32 heightNumber   = 1;
		uint32 normalNumber   = 1;
		uint32 specularNumber = 1;
        Renderer::getErrors();

		for (uint32 textureIndex = 0; textureIndex < mesh.material.textures.size(); textureIndex++) { LOG_SCOPE("Assigning texture");
			Ref<Texture> texture = mesh.material.textures[textureIndex];

			glActiveTexture(GL_TEXTURE0  + textureIndex);
            glBindTexture  (GL_TEXTURE_2D, texture->getID());

            switch (texture->type) {
				case TextureType::DIFFUSE : shader->setInt("texture_diffuse"  + toString( diffuseNumber), (int) textureIndex); continue;
				case TextureType::HEIGHT  : shader->setInt("texture_height"   + toString(  heightNumber), (int) textureIndex); continue;
				case TextureType::NORMAL  : shader->setInt("texture_normal"   + toString(  normalNumber), (int) textureIndex); continue;
				case TextureType::SPECULAR: shader->setInt("texture_specular" + toString(specularNumber), (int) textureIndex); continue;
				
				default: continue;
			}

        }
        Renderer::getErrors();

        glBindVertexArray(mesh.vertexArray->getID());
        //glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mesh.indexBuffer->getID());
        Renderer::getErrors();

        glDrawElements(GL_TRIANGLES, (int) mesh.indexBuffer->getSize(), GL_UNSIGNED_INT, nullptr);
        Renderer::getErrors();

        // unset everything
        glBindVertexArray(0);
        glUseProgram(0);
        glActiveTexture(GL_TEXTURE0);
        Renderer::getErrors();

    }

	/* Pseudo fragment shader
	* 
	* struct quad {
	*     textureSampler texture;
	*         
	* }
	* 
	* 
	* 
	* 
	*
	* 
	*/

    struct Quad2D{
        float  x,  y;
        float tx, ty;
        int   ti;

        Quad2D(float x, float y, float tx, float ty, int ti): x(x), y(y), tx(tx), ty(ty), ti(ti) {}
    };

    Vector<Quad2D> quad2DVertices;
    Vector< uint > quad2DIndices ;
    Vector<Ref<Texture>> quad2DTextures;
	void OpenGLRenderer::drawQuad2D(Ref<Texture> texture, Transform transform) {
        const auto& xPos = transform.position.x;
        const auto& yPos = transform.position.y;
        const auto& w    = transform.scale   .x;
        const auto& h    = transform.scale   .y;

        quad2DVertices.emplace_back( xPos + 0, yPos + h, 0.f, 0.f, quad2DTextures.size() );
        quad2DVertices.emplace_back( xPos + 0, yPos + 0, 0.f, 1.f, quad2DTextures.size() );
        quad2DVertices.emplace_back( xPos + w, yPos + 0, 1.f, 1.f, quad2DTextures.size() );
        quad2DVertices.emplace_back( xPos + 0, yPos + h, 0.f, 0.f, quad2DTextures.size() );
        quad2DVertices.emplace_back( xPos + w, yPos + 0, 1.f, 1.f, quad2DTextures.size() );
        quad2DVertices.emplace_back( xPos + w, yPos + h, 1.f, 0.f, quad2DTextures.size() );

        quad2DTextures.push_back(texture);

        auto size = quad2DIndices.size();

        quad2DIndices.emplace_back( size + 0 );
        quad2DIndices.emplace_back( size + 1 );
        quad2DIndices.emplace_back( size + 2 );
        quad2DIndices.emplace_back( size + 3 );
        quad2DIndices.emplace_back( size + 4 );
        quad2DIndices.emplace_back( size + 5 );
    }

    void OpenGLRenderer::flush(){
        Renderer::getErrors();

        auto shader = Shader::load(
            "quad2D",
            ReadFile("/home/samuel/Desktop/Projects/Hei/Hei/Resources/Shaders/quad2D.vert"),
            ReadFile("/home/samuel/Desktop/Projects/Hei/Hei/Resources/Shaders/quad2D.frag")
        );
        Renderer::getErrors();

        static auto mesh = Mesh({}, {}, Material(), VertexLayout({
            {"position", ShaderDataType::Float2},
            {"texCords", ShaderDataType::Float2},
            {"texIndex", ShaderDataType::Int   }
        }));

        Renderer::getErrors();
        mesh.vertexBuffer->setData(quad2DVertices.data(), quad2DVertices.size() * sizeof(Quad2D));
        mesh. indexBuffer->setData(quad2DIndices .data(), quad2DIndices .size() * sizeof( uint ));
        Renderer::getErrors();

        Renderer::getErrors();
        glUseProgram(shader->ID);
        Renderer::getErrors();
        LOG(toString(shader->ID), 3);

        static auto perv = glm::ortho(0.0f, 800.f, 0.0f, 500.f);
        shader->setMat4("projection", perv);

        Renderer::getErrors();

        uint32 textureNumber = 0;
        for(int textureIndex=0;textureIndex<quad2DTextures.size();textureIndex++){
            Ref<Texture> texture = quad2DTextures[textureIndex];

            glActiveTexture(GL_TEXTURE0  + textureIndex);
            glBindTexture  (GL_TEXTURE_2D, texture->getID());

            shader->setInt("textures["  + toString(textureNumber) + "]", textureIndex);
        }
        Renderer::getErrors();
        glBindVertexArray(mesh.vertexArray->getID());
        glDrawElements(GL_TRIANGLES, (int) mesh.indexBuffer->getSize(), GL_UNSIGNED_INT, nullptr);
        Renderer::getErrors();

        quad2DVertices.clear();
        quad2DIndices .clear();
        quad2DTextures.clear();

        quad2DVertices.reserve(32);
        quad2DIndices .reserve(32);
        quad2DTextures.reserve(32);
    }
	
	void OpenGLRenderer::clear() {
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	}

	void OpenGLRenderer::resetBuffers() {
		glActiveTexture(GL_TEXTURE0);
	}
}