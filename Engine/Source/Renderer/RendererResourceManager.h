#pragma once

#include "../Core/Logger.h"

namespace PetrolEngine{

    // TODO: consider if createShader should receive String or const char*

    class VertexLayout;
    class VertexBuffer;
    class Texture     ;
    class IndexBuffer ;
    class VertexArray ;
    class RendererAPI ;
    class Shader      ;

    /*
     * RendererResourceManager class provides info for renderer.
     * For example It says what should be created when you create
     * new vertex buffer (as those are only interfaces for API specific classes)
     */
    class RendererResourceManager {
    public:
        virtual VertexBuffer* createVertexBuffer(VertexLayout* layout, const void *data, int64 size) { LOG("Incomplete RRM.", 4); return nullptr; }
        virtual Texture     * createTexture     (                      const void* data, int64 size) { LOG("Incomplete RRM.", 4); return nullptr; }
        virtual IndexBuffer * createIndexBuffer (                      const void* data, int64 size) { LOG("Incomplete RRM.", 4); return nullptr; }
        virtual VertexArray * createVertexArray (                                                  ) { LOG("Incomplete RRM.", 4); return nullptr; }
        virtual RendererAPI * createRenderer    (                                                  ) { LOG("Incomplete RRM.", 4); return nullptr; }

        virtual Shader* createShader( String&& vertexSrc  ,
                                      String&& fragmentSrc,
                                      String&& geometrySrc  ) { LOG("Incomplete RRM.", 4); return nullptr; };
    };
}