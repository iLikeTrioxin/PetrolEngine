#include <PCH.h>

#include "Scene.h"
#include "./Renderer/Renderer.h"
#include "Components.h"
#include "./Core/Window.h"
#include "Entity.h"
#include "DebugTools.h"

namespace PetrolEngine {
    // TODO: Use nullptr instead of strcmp if possible
	Entity Scene::createEntity(const char* name) {
		Entity entity = { sceneRegistry.create(), this };
		
		entity.addComponent<Transform>();
		entity.addComponent<   Tag   >( strcmp(name, "") == 0 ? "New entity" : name, entt::null );

		return entity;
	}

	Entity Scene::createEntity(const char* name, entt::entity parent) {
		Entity entity = { sceneRegistry.create(), this };

		entity.addComponent<Transform>();
		entity.addComponent<   Tag   >( strcmp(name, "") == 0 ? "New entity" : name, parent );
		
		return entity;
	}

	Entity Scene::getEntityById(uint id) {
		return Entity((entt::entity)id, this);
	}

	void Scene::update(float a) {
		LOG_FUNCTION();
		auto cameras = sceneRegistry.group<Camera>(entt::get<Transform>);
		auto meshes  = sceneRegistry.group< Mesh >(entt::get<Transform>);
		
		for (auto camera : cameras) {
			LOG_SCOPE("Processing camera view");
			
			auto& cam = cameras.get<  Camera >(camera);
			auto& tra = cameras.get<Transform>(camera);
			
			cam.updateViewMatrix(tra.position);
			
			for (auto entity : meshes) {
				LOG_SCOPE("Rendering mesh");

				auto& mesh      = meshes.get<   Mesh  >(entity);
				auto  transform = meshes.get<Transform>(entity);
				auto  parent    = sceneRegistry.get<Tag>(entity).parent;
				
				if (parent != entt::null) {
					transform += sceneRegistry.get<Transform>(parent);
				}

				transform.updateTransformMatrix();
				
				Renderer::renderMesh(mesh, transform, cam);
			}
			
		}

        auto scripts = sceneRegistry.group<ExternalScript*>(entt::get<Transform>);

        for(auto& script : scripts){
            scripts.get<ExternalScript*>(script)->onUpdate();
        }
    }
}